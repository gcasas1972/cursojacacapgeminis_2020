/**
 * 
 */
package figuras;

/**
 * @author GAbrie
 * Esta es clase padre que va a establecer las pautas
 * para todas las figuras
 *
 */


public abstract class Figura implements Comparable<Figura>{
	private String nombre;
	private static int cantidadDeFiguras;

	//constructores
	public Figura() {
		super();
		cantidadDeFiguras ++;
	}

	public Figura(String nombre) {
		this();
		this.nombre = nombre;		
	}

	//accessors
	
	/**
	 * Obtieen el nombre de la figura
	 * @return
	 */
	public String getNombre() {
		return nombre;
	}

/**
 * Asigna el nombre al atributo	
 * @param nombre , correpnde al nombre de la figura
 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	

	
	public static int getCantidadDeFiguras() {
	return cantidadDeFiguras;
	}
	
	public static void clear() {
		cantidadDeFiguras =0;
	}

	public abstract float calcularPerimetro();
	public abstract float calcularSuperficie();

	//metodos de negocio
	@Override
	public boolean equals(Object obj) {
		boolean bln =false;
		if (obj instanceof Figura) {
			Figura fig =(Figura)obj;
			bln = fig.getNombre() !=null &&
				  fig.getNombre().equals(nombre);			
		}
		return bln;
	}

	@Override
	public int hashCode() {
		return nombre==null?0:nombre.hashCode();		
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("nombre=");
		sb.append(nombre);
		sb.append("\n--------------\n");
		return sb.toString();
	}

	@Override
	public int compareTo(Figura o) {		
		return (-1)*nombre.compareTo(o.getNombre());
	}
	
	
}
