package figuras;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import figuras.exceptions.FiguraException;

public class Cuadrado extends Figura {
	
	private float lado;
	
	
	//constructores
	
	public float getLado() {
		return lado;
	}

	public void setLado(float lado) throws FiguraException {
		if (lado<0)
			throw new FiguraException("El valor del lado no puede ser negativo ANIMAL");
		this.lado = lado;
	}

	public Cuadrado() {	}
	
	public Cuadrado(String pNombre, float pLado) throws FiguraException {
		super(pNombre);
		setLado(pLado);		
	}

	@Override
	public float calcularPerimetro() {
		return lado*4;
	}

	@Override
	public float calcularSuperficie() {
		return lado*lado;
	}

	@Override
	public boolean equals(Object obj) {
	
		return super.equals(obj) 		&&
				obj instanceof Cuadrado &&
				((Cuadrado)obj).getLado()==lado;
	}

	@Override
	public int hashCode() {
		return super.hashCode() + (int)lado;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(super.toString());
		sb.append(", lado=");
		sb.append(lado);
		return sb.toString();
	}
	
	

}
