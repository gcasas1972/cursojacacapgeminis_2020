package comparaciones;

import java.util.Set;
import java.util.TreeSet;

import figuras.Circulo;
import figuras.Cuadrado;
import figuras.Figura;
import figuras.exceptions.FiguraException;

public class ComparacionExample {

	public static void main(String[] args) throws FiguraException  {
		Set<String> listNombres = new TreeSet<String>();
		listNombres.add(new String("Gabriel"));
		listNombres.add(new String("Pedro"));
		listNombres.add(new String("Marina"));
		listNombres.add(new String("Paz"));
		listNombres.add(new String("Patricia"));
		listNombres.add(new String("Lycy"));
		listNombres.add(new String("Maria Cristina"));
		listNombres.add(new String("Nerea"));
		listNombres.add(new String("Marta"));
		listNombres.add(new String("Viviana"));
		int i =1;
		for (String nombre : listNombres) 
			System.out.println( i++ +" nombre=" + nombre);
	
		Set<Figura> figuras = new TreeSet<Figura>();
		figuras.add(new Cuadrado("cuadrado", 10));
		figuras.add(new Cuadrado("Mi cuadrado", 20));
		figuras.add(new Cuadrado("Tu cuadrado", 30));
		figuras.add(new Cuadrado("El cuadrado de todos", 100));
		figuras.add(new Cuadrado("El cuadrado de ellos", 200));
		figuras.add(new Circulo("ciculo", 10));
		figuras.add(new Circulo("Mi ciculo", 10));
		figuras.add(new Circulo("Tu ciculo", 10));
		figuras.add(new Circulo("Ahhh que ciculo", 10));
		figuras.add(new Circulo("Epa que ciculo...", 10));;
		i=1;
		for (Figura figura : figuras) {
			System.out.println(i++ + " figura=" + figura);
		}
		

	}

}
