package es.edu.capgemini.piedrapapeltijera.modelo;

import java.util.ArrayList;
import java.util.List;

public abstract class PiedraPapelTijeraFactory {
	public static final int PIEDRA 	= 1;
	public static final int PAPEL 	= 2;
	public static final int TIJERA 	= 3;

	protected String 								descripcionResultado	;
	private static List<PiedraPapelTijeraFactory> 	elementos				;
	protected String 								nombre					;
	protected int 									numero					;
	//constructor
	public PiedraPapelTijeraFactory(int pNumero, String pNombre) {
		super();
		this.numero = pNumero;
		this.nombre = pNombre;
	}
	//getter y setter
	public String getNombre() {					return nombre;			}
	public void setNombre(String nombre) {		this.nombre = nombre;	}
	
	public int getNumero() {					return numero;			}
	public void setNumero(int numero) {			this.numero = numero;	}
	
	public String getDescripcionResultado() {		return descripcionResultado;	}
	
	//metodo de negocio
	public abstract boolean isMe(int pNumero);
	public abstract int comparar(PiedraPapelTijeraFactory pPiedraPapelTijera);
	
	public static PiedraPapelTijeraFactory getInstace(int pNumero) {
		//el padre conoce a todos sus hijos
		elementos = new ArrayList<PiedraPapelTijeraFactory>();
		elementos.add(new Piedra());
		elementos.add(new Papel());
		elementos.add(new Tijera());
		
		for (PiedraPapelTijeraFactory piedraPapelTijeraFactory : elementos) {
			if(piedraPapelTijeraFactory.isMe(pNumero))
				return piedraPapelTijeraFactory;
		}
		return null;
	}
	
}















