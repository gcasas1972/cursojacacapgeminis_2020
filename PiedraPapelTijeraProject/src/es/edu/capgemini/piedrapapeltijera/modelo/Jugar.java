package es.edu.capgemini.piedrapapeltijera.modelo;

import java.util.Scanner;

public class Jugar {

	public static void main(String[] args) {
		
		System.out.println("vamos a jugar a piedra-papel-tijera vivaaa...!!!");
		//el numero que elige el usuario
		System.out.println("ingrese un nro 1-pedra, 2-papel, 3-tijera ");
		Scanner sc = new Scanner(System.in);
		int nro = sc.nextInt();
		
		//el numero que selecciona el ordenador
		
		int nroOrdenador = (int) (Math.random()*3) + 1;
		
		PiedraPapelTijeraFactory pptUsuario = PiedraPapelTijeraFactory.getInstace(nro);
		PiedraPapelTijeraFactory pptOrdenador = PiedraPapelTijeraFactory.getInstace(nroOrdenador);
		
		pptUsuario.comparar(pptOrdenador);
		
		System.out.println("\n\nEl resultado del juago es....");
		System.out.println("el usuario eligio..: "+ pptUsuario.getNombre());
		System.out.println("el ordenador eligio " + pptOrdenador.getNombre());
		System.out.println("resultado " + pptUsuario.getDescripcionResultado());
		sc.close();
	}

}
