package es.edu.capgemini.piedrapapeltijera.modelo;

public class Tijera extends PiedraPapelTijeraFactory {
	public Tijera() {
		this(TIJERA, "tijera");
	}

	public Tijera(int pNumero, String pNombre) {
		super(pNumero, pNombre);

	}

	@Override
	public boolean isMe(int pNumero) {
		return pNumero==TIJERA;
	}
	

	@Override
	public int comparar(PiedraPapelTijeraFactory pPiedraPapelTijera) {
		int result = 0;
		
		int numeroRecibido = pPiedraPapelTijera.getNumero();
		switch (numeroRecibido) {
		case PIEDRA:
			result = -1;
			descripcionResultado = nombre + " pierde con " + pPiedraPapelTijera.getNombre();
			break;
		case PAPEL:
			result = 1;
			descripcionResultado = nombre + " le gana a " + pPiedraPapelTijera.getNombre();
			break;
		default:
			result = 0;
			descripcionResultado = nombre + " empata con " + pPiedraPapelTijera.getNombre();
			
			break;
		}
		
		return result;
	}

}
