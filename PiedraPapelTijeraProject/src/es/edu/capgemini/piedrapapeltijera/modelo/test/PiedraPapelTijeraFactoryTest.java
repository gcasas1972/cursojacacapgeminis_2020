package es.edu.capgemini.piedrapapeltijera.modelo.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import es.edu.capgemini.piedrapapeltijera.modelo.Papel;
import es.edu.capgemini.piedrapapeltijera.modelo.Piedra;
import es.edu.capgemini.piedrapapeltijera.modelo.PiedraPapelTijeraFactory;
import es.edu.capgemini.piedrapapeltijera.modelo.Tijera;

class PiedraPapelTijeraFactoryTest {
	//lote de pruebas
	PiedraPapelTijeraFactory piedra	, 
							 papel	,  
							 tijera	;

	@BeforeEach
	void setUp() throws Exception {
		piedra = new Piedra();
		papel  = new Papel();
		tijera = new Tijera();
		
	}

	@AfterEach
	void tearDown() throws Exception {
		piedra 	= null;
		papel 	= null;
		tijera 	= null;
	}

	@Test
	void testGetInstacePiedra() {
		assertTrue(PiedraPapelTijeraFactory.getInstace(PiedraPapelTijeraFactory.PIEDRA) instanceof Piedra);
		
	}

	@Test
	void testGetInstacePiedra_false() {
		assertFalse(PiedraPapelTijeraFactory.getInstace(PiedraPapelTijeraFactory.PAPEL) instanceof Piedra);
		
	}

	@Test
	void testGetInstacePapel() {
		assertTrue(PiedraPapelTijeraFactory.getInstace(PiedraPapelTijeraFactory.PAPEL) instanceof Papel);
		
	}
	
	@Test
	void testGetInstaceTijera() {
		assertTrue(PiedraPapelTijeraFactory.getInstace(PiedraPapelTijeraFactory.TIJERA) instanceof Tijera);
	
	//comparaciones	
	//PIEDRA
	}
	@Test
	void testPiedraLeGanaTijera() {
		assertEquals(1, piedra.comparar(tijera));
	}
	@Test
	void testPiedraPierdeConPapel() {
		assertEquals(-1, piedra.comparar(papel));
	}
	@Test
	void testPiedraEmpataConPiedra() {
		assertEquals(0, piedra.comparar(new Piedra()));
	}
	
	
	//PAPEL
	@Test
	void testPapelLeGanaAPiedra() {
		assertEquals(1, papel.comparar(piedra));
	}
	@Test
	void testPapelPierdeConTijera() {
		assertEquals(-1, papel.comparar(tijera));
	}
	@Test
	void testPapelEmpataConPapel() {
		assertEquals(0, papel.comparar(new Papel()));
		assertEquals("papel empata con papel", papel.getDescripcionResultado());
	}
	
//TIJERA
	
}


















